_This file provides all the necessary steps to install and configure the
required technologies in this project._


Docker ( https://docs.docker.com/docker-for-windows/install/ )
==============================================================

REQUIREMENTS:

Docker for Windows runs on 64-bit Windows 10 Pro, Enterprise, and
Education; 1511 November update, Build 10586 or later. Docker plans to
support more versions of Windows 10 in the future.

WSL 2 BACKEND - Windows 10 64-bit: Home, Pro, Enterprise, or Education,
version 1903 (Build 18362 or higher). - Enable the WSL 2 feature on
Windows. For detailed instructions, refer to the Microsoft
documentation(
https://docs.microsoft.com/en-us/windows/wsl/install-win10 ). - The
following hardware prerequisites are required to successfully run WSL 2
on Windows 10:

-64-bit processor with Second Level Address Translation (SLAT) -4GB
system RAM -BIOS-level hardware virtualization support must be enabled
in the BIOS settings. For more information, see Virtualization.
-Download and install the Linux kernel update package(
https://docs.microsoft.com/en-us/windows/wsl/install-win10\#step-4---download-the-linux-kernel-update-package
).

INSTALL DOCKER DESKTOP ON WINDOWS:

1.  Double-click Docker Desktop Installer.exe to run the installer. If
    you haven’t already downloaded the installer (Docker Desktop
    Installer.exe), you can get it from Docker Hub. It typically
    downloads to your Downloads folder, or you can run it from the
    recent downloads bar at the bottom of your web browser.

2.  When prompted, ensure the Enable Hyper-V Windows Features or the
    Install required Windows components for WSL 2 option is selected on
    the Configuration page.

3.  Follow the instructions on the installation wizard to authorize the
    installer and proceed with the install.

4.  When the installation is successful, click Close to complete the
    installation process.

If your admin account is different to your user account, you must add
the user to the docker-users group. Run Computer Management as an
administrator and navigate to Local Users and Groups \> Groups \>
docker-users. Right-click to add the user to the group. Log out and log
back in for the changes to take effect.

START DOCKER DESKTOP:

1.  Docker Desktop does not start automatically after installation. To
    start Docker Desktop, search for Docker, and select Docker Desktop
    in the search results.

search for Docker app

2.  When the whale icon in the status bar stays steady, Docker Desktop
    is up-and-running, and is accessible from any terminal window.

whale on taskbar

If the whale icon is hidden in the Notifications area, click the up
arrow on the taskbar to show it. To learn more, see Docker Settings.

3.  When the initialization is complete, Docker Desktop launches the
    onboarding tutorial. The tutorial includes a simple exercise to
    build an example Docker image, run it as a container, push and save
    the image to Docker Hub.

Docker Quick Start tutorial

Congratulations! You are now successfully running Docker Desktop on
Windows.

If you would like to rerun the tutorial, go to the Docker Desktop menu
and select Learn.

PostgreSQL (Docker Image/Container) ( https://hub.docker.com/\_/postgres )
==========================================================================

On Command Prompt (Windows) run:

`docker pull postgres:13`

Use docker images to see downloaded image(s) and their information.

To create a container or start an instance use:

```
docker run --name pg -e POSTGRES\_PASSWORD=password -d -p 5432:5432
postgres:13
```


Use docker ps to see containers and their properties.

To run the container:

Open Docker Desktop -\> Start Postgres Container -\> Open CLI

OR

On Window's Command Prompt console use:

`docker exec -it pg /bin/bash`

You should now be able to use Unix/Linux commands here on docker
terminal.

To run PostgreSQL use:

`psql -U postgres`

To check installed extensions use `\dx` command.

To use python functions with PL/python use:

`CREATE EXTENSION plpython3u;`

Jupyter Notebooks
=================

You can install it with (on Window's Command Prompt ):

`pip install jupyter-notebook`

Congratulations, you have installed Jupyter Notebook! To run the
notebook, run the following command at the Terminal (Mac/Linux) or
Command Prompt (Windows):

`jupyter-notebook`

See Running the Notebook
(https://jupyter.readthedocs.io/en/latest/running.html\#running) for
more details.

Jupytext
========

You can install Jupytext with (on Window's Command Prompt ):

`pip install jupytext`

Then, restart your Jupyter server.

When Jupytext is installed, .py and files have a notebook icon. And you
can really open and run these files as notebooks.

For more installation details, see the install section in the
documentation (https://jupytext.readthedocs.io/en/latest/install.html).

pgAdmin 4 v5
============

Steps to Install PgAdmin 4 on Windows 10 :

1.  Download the installer
    (https://www.postgresql.org/ftp/pgadmin/pgadmin4/v5.3/windows/)
2.  Double-click the pgadmin4-5.3-x64.exe
3.  Follow on-screen installation wizard
4.  Restart your computer if asked

Once the installation completes, you can open it from Windows menu. It
will take a moment for the first run.

Add a New PostgreSQL Server to PgAdmin4:

1.  Click Add New Server from PgAdmin main menu.
2.  Give a new connection name
3.  Click Connection tab and enter the PostgreSQL server details
4.  Press Save button to save the configuration

Once connected, you can now start managing your server.
