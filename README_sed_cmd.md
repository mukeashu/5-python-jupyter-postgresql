**SEARCH AND REPLACE using `SED`**

_To convert the python functions to PL/python functions, use this `sed`
command in a unix/linux terminal:_

```
sed -i -e 's/def/CREATE FUNCTION/g' -e '$a$\$ LANGUAGE plpython3u;' -e 's/-\>/RETURNS/g' -e 's/: int/ integer/g' -e 's/int:/integer \nAS \$\$/g' filename.py
```
_To remove python comments from a file:_

` sed '/^\s*#/d' filename.py ` 
