**Basic Process Timeline**

_To run the project follow the given steps:_

_( Commands to be run on Window's command prompt )_

1. Open Jupyter Notebook

`C:\Users\ashum\DatabaseProject>jupyter-notebook`

Should open Jupyter Notebook on your browser `localhost:8888`

2. Write a simple python as shown in the test functions uploaded on GitLab.

3. Pair the notebook with Jupytext to get .py file from an .ipynb file

Go to File -> Jupytext -> Pair Notebook with **light Script**

Name and save the file. You should be able see a .py file of same name in the directory.

4. On Command Prompt,

`C:\Users\ashum\DatabaseProject>type filename.py`

should show you the file contents and can also be edited using `notepad` instead of `type`.

5. Remove the jupytext and other python comments _manually_ or use the command

`sed '/^\s*#/d' filename.py`

 if they exist.

6. Put the file in docker using, on Command Prompt,

`docker cp filename.py DOCKER_CONATINER_ID:/tmp`

To find ConatinerID use command `docker ps`.

7. Use the following commands to find the file in docker for further use.

```
C:\Users\ashum\DatabaseProject>docker exec -it DOCKER_CONTAINER_ID /bin/bash

root@43ab636346bf:/# cd tmp

root@43ab636346bf:/tmp# ls -l

- rwxr-xr-x 1 root root    398 Jun 14 22:20 Dummy.py
- rwxr-xr-x 1 root root    115 Jun 15 10:19 multiply.py
- rwxr-xr-x 1 root root    130 Jun 15 10:05 pymax.py...
```


8. To run the python function as PL/python function use the sed command in the bash terminal as provided.

`sed -i -e 's/def/CREATE FUNCTION/g' -e '$a$$ LANGUAGE plpython3u;' -e 's/->/RETURNS/g' -e 's/: int/ integer/g' -e 's/int:/integer \nAS $$/g' filename.py`

9. To make sure we can run PL/python functions in PostgreSQL use the following commands, _use `\dx` command in psql to check current extensions_.

```
# psql -U postgres

psql (13.3 (Debian 13.3-1.pgdg100+1))

Type "help" for help.

postgres=# CREATE EXTENSION plpyhton3u;

CREATE EXTENSION
```


You should be able to run PL/python on your postgreSQL now.

10. To load the converted file into postgreSQL use following command on docker bash as shown from before.

`#psql -f filename.py -U postgres`

File should be successfully loaded into psql.

11. To run the PL/python function such as the test functions provided use, for example

```
postgres=# SELECT (pymax(22,11));

pymax

-------
22
(1 row)
```


12. We have successfully run a simple python written in Jupyter Notebook in a postgreSQL database.




