_**Processes and Ideas for further work.**_

1. Using Jupyter Notebook as a docker image and container and NOT on Windows.

On Window's command prompt using,

`docker run -it --rm --name DOCKERNAME -p 8888:8888 jupyter/datascience-notebook`

AND,

`$ docker run --rm -p 8888:8888 -v -$(pwd) DOCKER_IMAGEID`

Go to `localhost:8888` and copy the token from the terminal window or follow one of the links provided on the terminal.

Go to docker bash terminal to install **jupytext**.

Using again, 

`pip install jupytext` _(see further in README_installconfig.md)_


> This should help with further ease of use as all the jupyter files and their pairs will be inherently on Docker and will not need to be loaded with the use of many different command prompt windows.
> Thus, also easier to load them on to postgreSQL database after using the `sed` commmand.


2. _(NOT IMPLEMENTED)_ Using `awk/sed/grep` command or some other tool such as **Notepad++** to extract just the the required python/PL/python functions from the targeted extraction file.

3. _(NOT IMPLEMENTED)_ Using different kinds of python functions with different data types such as _varchar_, _boolean_, etc. and comparing how they work.

A few PL/Python function examples of the same point:

```
CREATE FUNCTION greet (how text)

RETURNS SETOF greeting

AS $$

return ( [ how, "World" ], [ how, "PostgreSQL" ], [ how, "PL/Python" ] )

$$ LANGUAGE plpython3u;
```


AND,

```
CREATE FUNCTION greet (how text)

RETURNS SETOF greeting

AS $$

for who in [ "World", "PostgreSQL", "PL/Python" ]:

yield ( how, who )

$$ LANGUAGE plpython3u;
```


